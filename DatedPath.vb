﻿Public Class DatedPath

    Public Sub New(paths As ArrayList)

        mFullRawList = paths

    End Sub

    Public Class DateComparer

        Implements IComparer

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements IComparer.Compare
            Return New CaseInsensitiveComparer().Compare(
                         CType(y, PathInfo).WriteDate, CType(x, PathInfo).WriteDate)
        End Function

    End Class

    Private mFullRawList = Nothing
    Public ReadOnly Property FullRawList() As ArrayList
        Get
            Return mFullRawList
        End Get
    End Property

    Private mFullPathList = Nothing
    Public ReadOnly Property FullPathList() As ArrayList
        Get
            If mFullPathList Is Nothing Then
                mFullPathList = New ArrayList
                For Each item In FullRecordList
                    mFullPathList.Add(item.PathSpec)
                Next
            End If
            Return mFullPathList
        End Get
    End Property

    Private mFullDateList = Nothing
    Public ReadOnly Property FullDateList() As ArrayList
        Get
            If mFullDateList Is Nothing Then
                mFullDateList = New ArrayList
                For Each item In FullRecordList
                    mFullDateList.add(item.WriteDate)
                Next
            End If
            Return mFullDateList
        End Get
    End Property

    Private mFullRecordList = Nothing
    Public ReadOnly Property FullRecordList() As ArrayList
        Get
            If mFullRecordList Is Nothing Then ProcessLists()
            Return mFullRecordList
        End Get
    End Property

    Private Sub ProcessLists()

        mFullRecordList = New ArrayList
        For idx = 0 To FullRawList.Count - 1
            mFullRecordList.Add(New DatedPath.PathInfo(FullRawList(idx)))
        Next
        mFullRecordList.Sort(New DatedPath.DateComparer)

    End Sub

    Public Class PathInfo
        'Fields
        Private mPathSpec As String
        Private mWriteDate As Date

        Private mParent As String
        Public ReadOnly Property Parent As String
            Get
                Return Me.mParent
            End Get
        End Property

        Private mFolder As String
        Public ReadOnly Property Folder As String
            Get
                Return mFolder
            End Get
        End Property

        Private mFilename As String
        Public ReadOnly Property Filename As String
            Get
                Return mFilename
            End Get
        End Property

        Private mExt As String
        Public ReadOnly Property Ext As String
            Get
                Return mExt
            End Get
        End Property

        Private mNewVal As String
        Public ReadOnly Property NewVal As String
            Get
                Return mNewVal
            End Get
        End Property

        'Constructor
        Public Sub New(ByVal path As String)
            mPathSpec = path
            mWriteDate = System.IO.File.GetLastWriteTime(path)
            mParent = System.IO.Path.GetDirectoryName(path)
            mFolder = System.IO.Path.GetFileNameWithoutExtension(Parent)
            mFilename = System.IO.Path.GetFileName(path)
            mExt = System.IO.Path.GetExtension(path)
            mNewVal = String.Format("{0}\{1}", mFolder, mFilename)
        End Sub

        'Properties
        Public ReadOnly Property PathSpec As String
            Get
                Return Me.mPathSpec
            End Get
        End Property

        Public ReadOnly Property WriteDate As Date
            Get
                Return Me.mWriteDate
            End Get
        End Property

    End Class

End Class
