﻿Public Class SoftwareSuite

    '    Private Shared mSuiteDistribution = "\\nt-eib-10-6b16\Software\EIB Flow Lab"
    Private Shared mSuiteDistribution = "\\nt-eib-10-6b16\www\BetaDistribution"
    Public Shared ReadOnly Property SuiteDistribution As String
        Get
            Return mSuiteDistribution
        End Get
    End Property

    Private Shared mSoftwareList = Nothing
    Public Shared ReadOnly Property SoftwareList() As ArrayList
        Get
            If mSoftwareList Is Nothing Then ScanSoftware()
            Return mSoftwareList
        End Get
    End Property

    Private Shared mDateList = Nothing
    Public Shared ReadOnly Property DateList() As ArrayList
        Get
            If mDateList Is Nothing Then ScanSoftware()
            Return mDateList
        End Get
    End Property

    Private Shared Sub ScanSoftware()

        Dim lst = New ArrayList
        If System.IO.Directory.Exists(SuiteDistribution) Then
            For Each app In System.IO.Directory.GetFiles(SuiteDistribution, "*.application")
                lst.Add(app)
            Next
        End If
        Dim dp = New DatedPath(lst)

        mSoftwareList = New ArrayList
        For Each item In dp.FullPathList
            mSoftwareList.add(System.IO.Path.GetFileNameWithoutExtension(item))
        Next
        mDateList = New ArrayList
        For Each item In dp.FullDateList
            mDateList.add(item)
        Next

    End Sub

End Class
