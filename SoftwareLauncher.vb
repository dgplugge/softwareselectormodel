﻿Public Class SoftwareLauncher

    Public Sub New(app As String)

        mFullAppSpec = System.IO.Path.Combine(AppRoot, String.Format("{0}.application", app))
        If System.IO.File.Exists(mFullAppSpec) Then
            mAppName = String.Format("{0}.application", app)
            mValidApp = True
        End If

    End Sub

    Private mValidApp As Boolean = False
    Public ReadOnly Property ValidApp() As Boolean
        Get
            Return mValidApp
        End Get
    End Property

    Private mAppRoot As String = "\\nt-eib-10-6b16\www\BetaDistribution"
    Public ReadOnly Property AppRoot() As String
        Get
            Return mAppRoot
        End Get
    End Property

    Private mAppName As String
    Public ReadOnly Property AppName() As String
        Get
            Return mAppName
        End Get
    End Property

    Private mFullAppSpec As String
    Public ReadOnly Property FullAppSpec() As String
        Get
            Return mFullAppSpec
        End Get
    End Property

    Private mLogPath = Nothing
    Public ReadOnly Property LogPath() As String
        Get
            If mLogPath Is Nothing Then
                mLogPath = System.Environment.GetEnvironmentVariable("APPDATA")
                mLogPath = System.IO.Path.Combine(mLogPath, "Flow Control")
            End If
            Return mLogPath
        End Get
    End Property

    Public Sub InstallApp()

        If Not ValidApp Then Exit Sub

        Dim PVWCmd = New Process


        ' a new process is created from running the PV-Wave program
        PVWCmd.StartInfo.UseShellExecute = True
        ' redirect IO
        ' PVWCmd.StartInfo.RedirectStandardOutput = True
        '       PVWCmd.StartInfo.RedirectStandardError = True
        '      PVWCmd.StartInfo.RedirectStandardInput = True
        ' don't even bring up the console window
        PVWCmd.StartInfo.CreateNoWindow = False
        ' executable command line info

        PVWCmd.StartInfo.FileName = AppName

        '        PVWCmd.StartInfo.EnvironmentVariables("pvwave_setup_flag") = "skip"

        '        PVWCmd.StartInfo.RedirectStandardInput = True

        '        PVWCmd.StartInfo.WorkingDirectory = Dist_Str
        PVWCmd.StartInfo.WorkingDirectory = AppRoot
        Dim mLogFullSpec = System.IO.Path.Combine(String.Format("{0}\{1}.log", LogPath, AppName))
        PVWCmd.StartInfo.Arguments = String.Format(" > {0}", mLogFullSpec.ToString)

        PVWCmd.Start()

    End Sub



End Class
