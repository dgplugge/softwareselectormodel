﻿Imports Microsoft.Win32

Public Class OneClick

    Private Shared mOneClickList = Nothing
    Public Shared ReadOnly Property OneClickList() As ArrayList
        Get
            If mOneClickList Is Nothing Then RegistryScan()
            Return mOneClickList
        End Get
    End Property

    Private Shared mRegPath As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"

    Private Shared mUniqueNames

    Private Shared Sub AddItem(ByVal subregkey As RegistryKey)

        Dim item = subregkey.Name
        If mUniqueNames.Contains(item) Then Return
        mUniqueNames.Add(item)

        Dim keys As New ArrayList
        keys.Add("Contact")
        keys.Add("DisplayName")
        keys.Add("InstallSource")
        keys.Add("DisplayVersion")
        keys.Add("InstallDate")
        keys.Add("Publisher")
        keys.Add("UninstallString")

        Dim keyword
        Dim info As New Hashtable
        For Each keyword In keys
            If subregkey.GetValue(keyword) IsNot Nothing Then info.Add(keyword, subregkey.GetValue(keyword))
        Next
        mOneClickList.Add(info)

    End Sub

    Private Shared Sub ScanRegistryProducts(ByVal RegKey)

        Dim SubRegKey As RegistryKey
        Dim name As String
        Dim auth As String
        Dim source As String
        Dim pub As String
        If RegKey Is Nothing Then Return
        Dim item
        Dim uninstal
        For Each item In RegKey.GetSubKeyNames
            SubRegKey = RegKey.OpenSubKey(item, False)
            name = SubRegKey.GetValue("DisplayName")
            auth = SubRegKey.GetValue("Contact")
            source = SubRegKey.GetValue("InstallSource")
            pub = SubRegKey.GetValue("Publisher")
            uninstal = SubRegKey.GetValue("UninstallString")
            If uninstal IsNot Nothing Then If uninstal.ToLower.Contains("dfshim") Then AddItem(SubRegKey)
            If auth IsNot Nothing Then If auth.ToLower.Contains("plugge") Then AddItem(SubRegKey)
            If source IsNot Nothing Then If source.ToLower.Contains("nt-eib-10-6b16") Then AddItem(SubRegKey)
            If pub IsNot Nothing Then If pub.ToLower.Contains("flow control") Then AddItem(SubRegKey)
            If name IsNot Nothing Then If name.ToLower.Contains("pv-wave") Then AddItem(SubRegKey)
        Next

    End Sub

    ' dgp rev 3/6/2012
    Private Shared Sub RegistryScan()

        mUniqueNames = New ArrayList
        mOneClickList = New ArrayList
        Dim RegKey As RegistryKey
        Try
            RegKey = Registry.LocalMachine.OpenSubKey(mRegPath, False)
            ScanRegistryProducts(RegKey)
        Catch ex As Exception

        End Try
        Try
            RegKey = Registry.CurrentUser.OpenSubKey(mRegPath, False)
            ScanRegistryProducts(RegKey)
        Catch ex As Exception

        End Try

    End Sub


    Public Class OneClickRegEntry

        Public Sub New(reg As RegistryKey)

        End Sub

    End Class


End Class
