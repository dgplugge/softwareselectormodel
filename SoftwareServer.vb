﻿' Author: Donald G Plugge
' Title:
' Date: 12/13/2010
' Purpose:
Imports System.Xml.Linq

Public Class SoftwareServer

    Private Shared mServerUp As Boolean
    Private Shared mPingOnce As Boolean = False

    ' dgp rev 12/15/2010 
    Private Shared mServer As String = "NT-EIB-10-6B16"
    Public Shared ReadOnly Property Server As String
        Get
            Return mServer
        End Get
    End Property

    ' dgp rev 12/15/2010 
    Public Shared Function DistributionList() As ArrayList

        DistributionList = New ArrayList
        Dim UNC_Path As String = "\\" + Server + "\" + ShareDistribution + "\Software"
        If (System.IO.Directory.Exists(UNC_Path)) Then
            Dim item
            For Each item In System.IO.Directory.GetDirectories(UNC_Path)
                DistributionList.Add(item.name)
            Next
        End If

    End Function

    ' dgp rev 12/15/2010 
    Public Shared ReadOnly Property ServerUp As Boolean
        Get
            If Not mPingOnce Then
                Try
                    mPingOnce = True
                    mServerUp = My.Computer.Network.Ping(Server, 1000)
                Catch ex As Exception
                    mServerUp = False
                End Try
            End If
            Return mServerUp
        End Get
    End Property

    ' dgp rev 12/15/2010 
    Public Shared ReadOnly Property PathValid As Boolean
        Get
            If Not ServerUp Then Return False
            Return True
        End Get
    End Property

    ' dgp rev 12/15/2010 
    Public Shared ReadOnly Property DistributionRoot As String
        Get
            Return String.Format("\\{0}\{1}\Software", Server, ShareDistribution)
        End Get
    End Property

    ' dgp rev 12/15/2010 
    Public Shared ReadOnly Property XMLPath As String
        Get
            Return String.Format("\\{0}\{1}\Software", Server, ShareDistribution)
        End Get
    End Property

    ' dgp rev 12/15/2010 
    Private Shared mShareDistribution As String = "Distribution"
    Public Shared ReadOnly Property ShareDistribution As String
        Get
            Return mShareDistribution
        End Get
    End Property

    Private Shared mCurName = "Configuration"
    Public Shared ReadOnly Property CurName() As String
        Get
            Return mCurName
        End Get
    End Property


    Private Shared mScanList As Hashtable

    Public Shared Sub LoadConfig()

        Dim Xele As XElement = XElement.Load(System.IO.Path.Combine(XMLPath, CurName + ".xml"))
        mConfigTable = New Hashtable

        For Each app In Xele.Elements

            Dim tmp = New SoftwareInfo(app.Attribute("Name").Value, app.Attribute("Active").Value, app.Attribute("Test").Value)
            If mConfigTable.ContainsKey(tmp.Name) Then
                mConfigTable(tmp.Name) = tmp
            Else
                mConfigTable.Add(tmp.Name, tmp)
            End If
        Next

    End Sub

    ' dgp rev 11/16/2010 
    Public Shared Function ModList(ByVal key As String, ByVal value As String) As Boolean

        ModList = False

        Dim Xele As XElement = XElement.Load(System.IO.Path.Combine(XMLPath, CurName + ".xml"))

        For Each SubAtt In ConfigElements.Descendants.Attributes(key)
            ModList = True
            SubAtt.SetValue(value)
        Next

        Xele.Save(System.IO.Path.Combine(XMLPath, CurName + ".xml"))

    End Function

    ' dgp rev 11/16/2010 
    Public Shared Function TestVersion() As String

        TestVersion = ""

        For Each SubAtt In ConfigElements.Descendants.Attributes("TestVersion")
            TestVersion = SubAtt.Value
        Next

    End Function

    ' dgp rev 8/1/2011 Username
    Private Shared mUserName As String = System.Environment.GetEnvironmentVariable("username")
    Public Shared Property UserName As String
        Get
            Return mUserName
        End Get
        Set(value As String)
            mUserName = value
            mCurrentLicense = Nothing
            mPersonalVersion = Nothing
        End Set
    End Property

    Private Shared mServerDistRoot = Nothing
    Private Shared Sub GetServerDistRoot()

        Dim Server = String.Format("\\{0}", mServer)

        Dim path = System.IO.Path.Combine(Server, "Distribution")
        If (System.IO.Directory.Exists(path)) Then
            path = System.IO.Path.Combine(path, "Software")
            If (System.IO.Directory.Exists(path)) Then
                If System.IO.Directory.GetDirectories(path).Length > 0 Then
                    mServerDistRoot = path
                End If
            End If
        End If

    End Sub

    ' dgp rev 6/24/09 Server Distribution Root
    Private Shared ReadOnly Property ServerDistRoot() As String
        Get
            If mServerDistRoot Is Nothing Then GetServerDistRoot()
            Return mServerDistRoot
        End Get
    End Property


    ' dgp rev 6/24/09 Retrieve Server Distribution List
    Private Shared Function GetServerDistList() As ArrayList

        GetServerDistList = New ArrayList

        If ServerDistRoot IsNot Nothing Then
            Dim item
            For Each item In System.IO.Directory.GetDirectories(ServerDistRoot)
                GetServerDistList.Add(System.IO.Path.GetFileName(item))
            Next
        End If

    End Function

    ' dgp rev 6/24/09 Server Distribution List
    Private Shared mServerDistList = Nothing
    Public Shared ReadOnly Property ServerDistList() As ArrayList
        Get
            If mServerDistList Is Nothing Then mServerDistList = GetServerDistList()
            Return mServerDistList
        End Get
    End Property

    ' dgp rev 6/23/09 Retrive the complete server distribution list
    Private Shared Function Check_Dist_Server() As Boolean

        Dim Server = String.Format("\\{0}", mServer)

        Check_Dist_Server = False
        Dim path = System.IO.Path.Combine(Server, "Distribution")
        If (System.IO.Directory.Exists(path)) Then
            path = System.IO.Path.Combine(path, "PVW License")
            If (System.IO.Directory.Exists(path)) Then
                path = System.IO.Path.Combine(path, "Current")
                Check_Dist_Server = (System.IO.Directory.Exists(path))
            End If
        End If

    End Function

    Private Shared mServerAnyDist = Nothing
    Private Shared ReadOnly Property ServerAnyDist() As String
        Get
            If mServerAnyDist Is Nothing Then mServerAnyDist = Check_Dist_Server()
            Return mServerAnyDist
        End Get
    End Property

    Private Shared mDownloadedDist = Nothing

    ' dgp rev 8/1/2011 Current Version  
    Private Shared mCurrentLicense = Nothing
    Public Shared ReadOnly Property CurrentLicense
        Get
            If mCurrentLicense Is Nothing Then
                If PersonalVersion = "" Then
                    If DefaultFlag Then
                        mCurrentLicense = DefaultVersion
                    End If
                Else
                    mCurrentLicense = PersonalVersion
                End If
            End If
            Return mCurrentLicense
        End Get
    End Property

    ' dgp rev 8/2/2011 Personal Version
    Private Shared mPersonalVersion = Nothing
    Public Shared ReadOnly Property PersonalVersion As String
        Get
            If mPersonalVersion Is Nothing Then PersonalScan()
            Return mPersonalVersion
        End Get
    End Property

    ' dgp rev 8/2/2011
    Public Shared ReadOnly Property PersonalFlag As Boolean
        Get
            If mPersonalVersion Is Nothing Then PersonalScan()
            Return Not (mPersonalVersion = "")
        End Get
    End Property

    ' dgp rev 11/16/2010 
    Private Shared Sub PersonalScan()

        mPersonalVersion = ""
        For Each SubXele In ConfigElements.Descendants(UserName)
            For Each SubAtt In SubXele.Descendants.Attributes("CurVersion")
                mPersonalVersion = SubAtt.Value
            Next
        Next

    End Sub

    Private Shared mConfigElements As XElement
    Public Shared ReadOnly Property ConfigElements As XElement
        Get
            If mConfigElements Is Nothing Then mConfigElements = XElement.Load(System.IO.Path.Combine(XMLPath, mCurName + ".xml"))
            Return mConfigElements
        End Get
    End Property

    Private Shared SubAtt As Xml.Linq.XAttribute
    Private Shared SubXele As Xml.Linq.XElement

    ' dgp rev 8/2/2011 Personal Version
    Private Shared mDefaultFlag As Boolean = False
    Private Shared mDefaultVersion As String = Nothing
    Public Shared ReadOnly Property DefaultVersion As String
        Get
            If mDefaultVersion Is Nothing Then DefaultScan()
            Return mDefaultVersion
        End Get
    End Property

    ' dgp rev 8/2/2011
    Public Shared ReadOnly Property DefaultFlag As Boolean
        Get
            If mDefaultVersion Is Nothing Then DefaultScan()
            Return mDefaultFlag
        End Get
    End Property

    ' dgp rev 11/16/2010 
    Private Shared Sub DefaultScan()

        mDefaultVersion = ""
        mDefaultFlag = False
        For Each SubXele In ConfigElements.Descendants("default")
            For Each SubAtt In SubXele.Descendants.Attributes("CurVersion")
                mDefaultVersion = SubAtt.Value
                mDefaultFlag = Not mDefaultVersion = ""
            Next
        Next

    End Sub

    ' dgp rev 11/16/2010 
    Public Shared Function CheckList(ByVal key As String) As Boolean

        CheckList = False

        Dim item
        For Each item In ConfigElements.Descendants.Attributes(key)
            CheckList = True
        Next

    End Function

    ' dgp rev 11/16/2010 
    Public Shared Function ReadList(ByVal key As String) As String

        Dim conf_file As String
        ReadList = ""
        If System.IO.Directory.Exists(XMLPath) Then
            conf_file = System.IO.Path.Combine(XMLPath, mCurName + ".xml")
            If System.IO.File.Exists(conf_file) Then
                Dim Xele As XElement = XElement.Load(conf_file)
                Dim item
                ReadList = ""
                For Each item In Xele.Descendants.Attributes(key)
                    ReadList = item.value
                Next
            End If
        End If

    End Function

    ' dgp rev 11/16/2010 
    Public Shared Sub SaveList(ByVal CurVer)

        mScanList = New Hashtable
        mScanList.Add("CurVersion", CurVer)
        mScanList.Add("TestVersion", "YY")

        Dim node As XElement
        Dim xml = New XElement("FlowControl")
        Dim item As DictionaryEntry
        For Each item In mScanList
            node = New XElement(XName.Get("Settings"),
               New XAttribute(XName.Get(item.Key), XName.Get(item.Value)),
               New XElement(XName.Get("Keyword"), XName.Get("Value")))
            xml.Add(node)
        Next

        xml.Save(System.IO.Path.Combine(XMLPath, mCurName + ".xml"))

    End Sub

    ' dgp rev 11/16/2010 
    Public Shared Sub SaveConfig()

        Dim node As XElement
        Dim xml = New XElement("FlowControl")
        Dim item As DictionaryEntry
        For Each item In ConfigTable

            Dim info As SoftwareInfo = item.Value
            node = New XElement(XName.Get("Application"),
               New XAttribute(XName.Get("Name"), XName.Get(info.Name)),
               New XAttribute(XName.Get("Active"), XName.Get(info.Active)),
               New XAttribute(XName.Get("Test"), XName.Get(info.Test)))
            xml.Add(node)
        Next

        xml.Save(System.IO.Path.Combine(XMLPath, mCurName + ".xml"))

    End Sub


    Private Shared mConfigTable = Nothing
    Public Shared ReadOnly Property ConfigTable() As Hashtable
        Get
            If mConfigTable Is Nothing Then LoadConfig()
            Return mConfigTable
        End Get
    End Property

    Public Class SoftwareInfo

        Private mName As String
        Public ReadOnly Property Name() As String
            Get
                Return mName
            End Get
        End Property

        Private mTest As String
        Public ReadOnly Property Test() As String
            Get
                Return mTest
            End Get
        End Property

        Private mActive As String
        Public ReadOnly Property Active() As String
            Get
                Return mActive
            End Get
        End Property

        Public Sub New(name As String, active As String, test As String)

            mName = name
            mActive = active
            mTest = test

        End Sub

    End Class

    Public Shared Sub Save(name As String, active As String, test As String)

        Dim tmp As New SoftwareInfo(name, active, test)
        If ConfigTable Is Nothing Then mConfigTable = New Hashtable
        mOrderedKeys = Nothing
        If ConfigTable.ContainsKey(tmp.Name) Then
            ConfigTable(tmp.Name) = tmp
        Else
            ConfigTable.Add(tmp.Name, tmp)
        End If

    End Sub

    Private Shared mOrderedKeys = Nothing
    Public Shared ReadOnly Property OrderedKeys() As ArrayList
        Get
            If mOrderedKeys Is Nothing Then
                Dim item As DictionaryEntry
                mOrderedKeys = New ArrayList
                For Each item In ConfigTable
                    mOrderedKeys.Add(item.Key)
                Next
            End If
            Return mOrderedKeys
        End Get
    End Property

    Private Shared mActiveList As ArrayList
    Public Shared ReadOnly Property ActiveList() As ArrayList
        Get
            mActiveList = New ArrayList
            For Each item In OrderedKeys
                mActiveList.Add(ConfigTable(item).Active)
            Next
            Return mActiveList
        End Get
    End Property

    Private Shared mTestList As ArrayList
    Public Shared ReadOnly Property TestList() As ArrayList
        Get
            mTestList = New ArrayList
            For Each item In OrderedKeys
                mTestList.Add(ConfigTable(item).Test)
            Next
            Return mTestList
        End Get
    End Property

    Public Shared Sub Delete(key As String)

        Dim tmp As New Hashtable
        Dim item As DictionaryEntry
        For Each item In ConfigTable
            If Not item.Key = key Then tmp.Add(item.Key, item.Value)
        Next
        mConfigTable = tmp
        mOrderedKeys = Nothing

    End Sub

End Class
